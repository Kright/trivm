use super::trit::Trit;

pub trait TritsWord where Self: std::marker::Sized {
    fn value(&self) -> i32;

    fn values_count() -> i32;

    fn max_value() -> i32 {
        (Self::values_count() - 1) / 2
    }

    fn new(value: i32) -> Self;

    fn wrap(value: i32) -> (Self, i32) {
        if value <= Self::max_value() && value >= -Self::max_value() {
            return (Self::new(value), 0);
        }

        if value > 0 {
            let v = (value - Self::max_value()) % Self::values_count() + Self::max_value();
            (Self::new(v), value - v)
        } else {
            let v = -((-value - Self::max_value()) % Self::values_count() + Self::max_value());
            (Self::new(v), value - v)
        }
    }

    fn neg(self) -> Self {
        Self::new(-self.value())
    }

    fn mul_short(self, other: Self) -> Self {
        Self::new(self.value() * other.value())
    }

    fn mul(self, other: Self) -> (Self, Self) {
        let (lower, remainder) = Self::wrap(self.value() * other.value());
        (lower, Self::new(remainder / Self::values_count()))
    }

    fn add(self, other: Self) -> (Self, Trit) {
        let (lower, remainder) = Self::wrap(self.value() + other.value());
        (lower, Trit::new(remainder / Self::values_count()))
    }

    fn sub(self, other: Self) -> (Self, Trit) {
        let (lower, remainder) = Self::wrap(self.value() - other.value());
        (lower, Trit::new(remainder / Self::values_count()))
    }
}
