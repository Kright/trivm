use super::TritsWord;

#[derive(Debug, Copy, Clone)]
pub struct MachineWord {
    value: i16,
    // 9 trits
}

impl TritsWord for MachineWord {
    fn value(&self) -> i32 {
        self.value as i32
    }

    fn values_count() -> i32 {
        19683
    }

    fn new(value: i32) -> Self {
        MachineWord { value: value as i16 }
    }
}
