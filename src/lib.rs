mod machine_word;
mod modular;
mod trit;

pub use machine_word::MachineWord;
pub use modular::TritsWord;
pub use trit::Trit;
