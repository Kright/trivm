use trivm::MachineWord;
use trivm::TritsWord;

fn main() {
    println!("Hello, world!");

    let (lo, hi) = MachineWord::new(3).mul(MachineWord::new(4));

    println!("{:?}, {:?}", lo, hi);
}
