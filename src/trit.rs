use super::modular::TritsWord;

#[derive(Debug, Copy, Clone)]
pub enum Trit {
    Positive,
    Zero,
    Negative,
}

impl TritsWord for Trit {
    fn value(&self) -> i32 {
        match self {
            Trit::Positive => 1,
            Trit::Zero => 0,
            Trit::Negative => -1,
        }
    }

    fn values_count() -> i32 {
        3
    }

    fn new(v: i32) -> Trit {
        match v {
            -1 => Trit::Negative,
            0 => Trit::Zero,
            1 => Trit::Positive,
            _ => unimplemented!(),
        }
    }
}
